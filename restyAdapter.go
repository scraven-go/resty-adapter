package restyAdapter

import (
	"github.com/go-resty/resty"
	HttpMessage "gitlab.com/scraven-go/http-message"
)

type Client struct {
	RestyClient 		*resty.Client
}

func (adapter *Client) getClient() *resty.Client {
	if adapter.RestyClient == nil {
		adapter.RestyClient = resty.New()
	}

	return adapter.RestyClient
}

func (adapter *Client) SendRequest(req *HttpMessage.Request) (response HttpMessage.Response, err error) {
	restyRequest := adapter.getClient().R()
	restyRequest.SetHeaders(req.Headers.GetConsolidatedMap())
	restyRequest.Body = req.Body

	var restyResponse *resty.Response

	switch req.Method {
	case HttpMessage.GetRequest:
		restyResponse, err = restyRequest.Get(req.Destination)
	case HttpMessage.PostRequest:
		restyResponse, err = restyRequest.Post(req.Destination)
	}

	if err != nil {
		return
	}

	response.Body = restyResponse.String()

	for name, valueSlice := range restyResponse.Header() {
		for _, value := range valueSlice {
			response.Headers.Add(&HttpMessage.Header{
				Name:  name,
				Value: value,
			})
		}
	}

	response.StatusCode = restyResponse.StatusCode()

	return
}
